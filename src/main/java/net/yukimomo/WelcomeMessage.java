package net.yukimomo;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class WelcomeMessage extends JavaPlugin implements Listener {

	public void onEnable(){
		super.onEnable();
		getServer().getPluginManager().registerEvents(this,this);
    	getCommand("wm").setExecutor(new WelcomeMessageCommandExecutor(this));
		this.saveDefaultConfig();
	}
	@Override
	public void onDisable(){
		super.onDisable();
	}
	@Override
	public void onLoad(){
		super.onLoad();
	}
	@EventHandler
	public void onLogin(PlayerJoinEvent event){
		String msg = getConfig().getString("Message");
		String title = getConfig().getString("Title");
		String subtitle = getConfig().getString("SubTitle");
		String joinMsg = getConfig().getString("JoinMessage");

		msg =ChatColor.translateAlternateColorCodes('&',util(msg,event.getPlayer()));
		title = ChatColor.translateAlternateColorCodes('&',util(title,event.getPlayer()));
		subtitle =ChatColor.translateAlternateColorCodes('&',util(subtitle,event.getPlayer()));
		joinMsg =ChatColor.translateAlternateColorCodes('&',util(joinMsg,event.getPlayer()));

		event.getPlayer().sendMessage(msg);
		event.setJoinMessage(joinMsg);
    	event.getPlayer().sendTitle(title, subtitle, 10, 70, 20);

	}

	@EventHandler
	public void onLogoff(PlayerQuitEvent event){
		String quitMsg = getConfig().getString("QuitMessage");

		quitMsg = ChatColor.translateAlternateColorCodes('&',util(quitMsg,event.getPlayer()));

		event.setQuitMessage(quitMsg);
	}

	public String util(String string,Player player){
		string = string.replaceAll("%player", player.getName());
		string = string.replaceAll("%server", getServer().getServerName());
		string = string.replaceAll("%online", Integer.toString(getServer().getOnlinePlayers().size()));
		string = string.replaceAll("%timeplayed", Float.toString((float)player.getStatistic(Statistic.PLAY_ONE_TICK)/1000/60) + "m");
		string = string.replaceAll("%lastdeath", Float.toString((float)player.getStatistic(Statistic.TIME_SINCE_DEATH)/1000/60) + "m");
		string = string.replaceAll("%walked", Float.toString((float)player.getStatistic(Statistic.WALK_ONE_CM)/10) + "m");
		string = string.replaceAll("%mobskilled", Integer.toString(player.getStatistic(Statistic.MOB_KILLS)));

		return string;
	}

}