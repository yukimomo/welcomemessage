package net.yukimomo;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class WelcomeMessageCommandExecutor implements CommandExecutor {
	private final WelcomeMessage plugin;
	public WelcomeMessageCommandExecutor(WelcomeMessage ref){
		plugin = ref;
	}
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(cmd.getName().equalsIgnoreCase("wm")){
			if(args[0].toUpperCase().equals("RL")){
				plugin.reloadConfig();
	      		sender.sendMessage("config.yml reloaded");
			}else if(args[0].toUpperCase().equals("SET")){
				String msg= "";
				for(String string : args) {
					if(string.toUpperCase().equals("SET")){
					}else{
						msg = msg + " " + string;
					}
				}
				plugin.getConfig().set("message",msg);
				plugin.saveConfig();
				plugin.reloadConfig();
			}
      		return true;
		}
  		return false;
	}
}
